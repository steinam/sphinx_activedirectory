#globale Variablen und Datenstrukturen
$DebugPreference = "Continue"
$domainInfos = get-ADDomain 
$baseDN = "OU=_CREATIVE,"+$domainInfos.DistinguishedName
$OU_HList = @{} #Aufname der HauptOUs
$OU_UList = @{} #Aufnahme der UnterOUs
$Group_List = @{} #Aufnahme der Gruppen


#csv-Datei braucht zeilenumbruch am Ende
$result = Import-Csv C:\temp\ad_struktur_eng.csv

#$result



function prepareUser{
<#
.synopsis
    Adds properties to each User-Object to handle AD-Management better
    Groups
    SamAccountName
    DistinguishedName

    Values for this properties are sometimes set in other functions
#>

    begin{
        Write-Debug "Adding Properties"
    }

    process{
        #Gruppenzugehörigkeit
        $myGroups = @() #ArrayList for adding GroupName to User-Object
        Add-Member -InputObject $_ -Name Gruppen -Value $myGroups -MemberType NoteProperty
        
        $myGroups += $_.Globale_Gruppe1

        if($_.Globale_Gruppe2 -ne "")
        {
            $myGroups += $_.Globale_Gruppe2

        }

        if($_.Globale_Gruppe3 -ne "")
        {
            $myGroups += $_.Globale_Gruppe3

        }

        $_.Gruppen = $myGroups


        #DistinguishedName
        Add-Member -InputObject $_ -Name DName -Value "" -MemberType NoteProperty
        

    }





}


function set-OUs
{
    
    begin{
        Write-Debug "Adding OU-Infos zu imported Objects"
    }

    process{
       # Write-Debug "in process"
    
    	#NAme der OU
		#Add-Member -InputObject $_ -Name OUName -Value "" -MemberType NoteProperty
       
        #Objekte der Pipeline erhalten eine neue Eigenschaft
        #DistinguishedName 
		#Add-Member -InputObject $_ -Name DName -Value "" -MemberType NoteProperty
        
        #Haupt-OUs werden immer angelegt
        $OUName = $_.HauptOU 
		$OU_DN_PATH = "" + $baseDN			
		$OU_HList[$OUName] = $OU_DN_PATH	

	    #Setzen des DistiguishedName zur einfacheren Anlage des Users
        $_.DName = "OU="+ $_.HauptOU +", " + $baseDN



        #falls UnterOU vorhanden
        if($_.UnterOU -ne "")
        {
            $OU_DN_PATH = "OU="+$_.HauptOU+"," + $baseDN 
			$OUName = $_.UnterOU
        	$OU_UList[$OUName] = $OU_DN_PATH	
            #Dname für Unter-OUs 
            $_.DName = "OU=" +$_.UnterOU +"," + $OU_DN_PATH 	
		}

    }
    
    end{

         foreach($element in $OU_HList.Keys)
        {
	        New-ADOrganizationalUnit -Name $element -Path $OU_HList[$element] -ProtectedFromAccidentalDeletion $False
        }


        foreach($element in $OU_UList.Keys)
        {
	        New-ADOrganizationalUnit -Name $element -Path $OU_UList[$element] -ProtectedFromAccidentalDeletion $False

        }


        Write-Debug "Finished"
    }
}



function set-Groups
{
    begin{
    
        $Group_List.Clear()
        New-ADOrganizationalUnit -Name Gruppen -Path $baseDN -ProtectedFromAccidentalDeletion $False

    }

    process{
    
        $Group_List[$_.Globale_Gruppe1]=$_.Globale_Gruppe1

        if($_.Globale_Gruppe2 -ne "")
        {
            $Group_List[$_.Globale_Gruppe2]=$_.Globale_Gruppe2
        }

        if($_.Globale_Gruppe3 -ne "")
        {
            $Group_List[$_.Globale_Gruppe3]=$_.Globale_Gruppe3
        }

    }

    end{
       
        #New-ADGroup -name Personal.G -Path "OU=gruppen, OU= _CREATIVE, DC=CREATIV, DC=STE"
        $Group_List
         foreach($element in $Group_List.Keys)
        {
            $path = "OU=Gruppen, " + $baseDn + ""
	        New-ADGroup -Name $Group_List[$element] -Path $path  -GroupScope Global
        }

    }
}


function remove-AllOUsAndGroups
{
    foreach($element in $OU_HList.Keys)
    {
        $DN = "OU="+ $element+ "," + $OU_HList[$element]

        Write-Host $DN
        #Remove-ADOrganizationalUnit -Identity "'OU='+ $element + ',' + $OU_HList[$element]" -confirm:$false -Recursive:$true
        Remove-ADOrganizationalUnit -Identity $DN -confirm:$false -Recursive:$true
    } 
    
    $Gruppen = "OU=Gruppen, " + $baseDN + ""
    Remove-ADOrganizationalUnit -Identity $Gruppen -confirm:$false -Recursive:$true
}


function  set-User
{
    begin{
        Write-Debug "Adding User"
    }

    process{
    
        #write-host $_.DName
        New-ADUser -Path $_.DName -SamAccountName $_.Anmeldename -Name $_.Name  -AccountPassword (ConvertTo-SecureString -force -AsPlainText "Pa$$w0rd") -ChangePasswordAtLogon $true
        #New-ADUser -Path "OU=SoftwareUndTraining, OU=_CREATIVE,DC=CREATIV,DC=STE" -name "steinam" -SamAccountName "Steinam2" -AccountPassword (ConvertTo-SecureString -Force -AsPlainText "Pa$$w0rd")
    
    
    
    }


    end{
        write-debug "finished adding user"
    }


}



function addUserToGroups
{

begin{
    Write-Debug "Adding User to Groups"

}



process{

    foreach($gruppe in $_.Gruppen)
    {
        

        #Add-ADGroupMember SvcAccPSOGroup SQL01,SQL02 
        #Adds the user accounts with SamAccountNames SQL01,SQL02 to the group SvcAccPSOGroup.

        Add-ADGroupMember $gruppe $_.Anmeldename

    }
}

end{}

}



############## Ablaufsteuerung ###########





# Auslesen der OU-Namen
$result | prepareUser
$result | set-OUs
#Anlegen der Gruppen
$result | set-Groups
$result | set-User
$result | addUserToGroups


#remove-AllOUsAndGroups


