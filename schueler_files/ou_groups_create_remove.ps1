#globale Variablen und Datenstrukturen
$DebugPreference = "Continue"
$domainInfos = get-ADDomain 
$baseDN = "OU=_CREATIVE,"+$domainInfos.DistinguishedName
$OU_HList = @{} #Aufname der HauptOUs
$OU_UList = @{} #Aufnahme der UnterOUs
$Group_List = @{} #Aufnahme der Gruppen


$result = Import-Csv C:\temp\ad_struktur_eng.csv

#$result

function set-OUs
{
    
    begin{
        Write-Debug "Adding OU-Infos zu imported Objects"
    }

    process{
       # Write-Debug "in process"
    
    	#NAme der OU
		#Add-Member -InputObject $_ -Name OUName -Value "" -MemberType NoteProperty
        #Pfad von 
		#Add-Member -InputObject $_ -Name OU_DN_PATH -Value "" -MemberType NoteProperty
        
        #Haupt-OUs werden immer angelegt
        $OUName = $_.HauptOU 
		$OU_DN_PATH = "" + $baseDN
			
		$OU_HList[$OUName] = $OU_DN_PATH	

	
        #falls UnterOU vorhanden
        if($_.UnterOU -ne "")
        {
            $OU_DN_PATH = "OU="+$_.HauptOU+"," + $baseDN 
			$OUName = $_.UnterOU
        	$OU_UList[$OUName] = $OU_DN_PATH	 	
		}

    }
    
    end{

         foreach($element in $OU_HList.Keys)
        {
	        New-ADOrganizationalUnit -Name $element -Path $OU_HList[$element] -ProtectedFromAccidentalDeletion $False
        }


        foreach($element in $OU_UList.Keys)
        {
	        New-ADOrganizationalUnit -Name $element -Path $OU_UList[$element] -ProtectedFromAccidentalDeletion $False

        }


        Write-Debug "Finished"
    }
}



function set-Groups
{
    begin{
    
        $Group_List.Clear()
        New-ADOrganizationalUnit -Name Gruppen -Path $baseDN -ProtectedFromAccidentalDeletion $False

    }

    process{
    
        $Group_List[$_.Globale_Gruppe1]=$_.Globale_Gruppe1

        if($_.Globale_Gruppe2 -ne "")
        {
            $Group_List[$_.Globale_Gruppe2]=$_.Globale_Gruppe2
        }

        if($_.Globale_Gruppe3 -ne "")
        {
            $Group_List[$_.Globale_Gruppe3]=$_.Globale_Gruppe3
        }

    }

    end{
       
        #New-ADGroup -name Personal.G -Path "OU=gruppen, OU= _CREATIVE, DC=CREATIV, DC=STE"
        $Group_List
         foreach($element in $Group_List.Keys)
        {
            $path = "OU=Gruppen, " + $baseDn + ""
	        New-ADGroup -Name $Group_List[$element] -Path $path  -GroupScope Global
        }

    }
}


function remove-AllOUsAndGroups
{
    foreach($element in $OU_HList.Keys)
    {
        $DN = "OU="+ $element+ "," + $OU_HList[$element]

        Write-Host $DN
        #Remove-ADOrganizationalUnit -Identity "'OU='+ $element + ',' + $OU_HList[$element]" -confirm:$false -Recursive:$true
        Remove-ADOrganizationalUnit -Identity $DN -confirm:$false -Recursive:$true
    } 
    
    $Gruppen = "OU=Gruppen, " + $baseDN + ""
    Remove-ADOrganizationalUnit -Identity $Gruppen -confirm:$false -Recursive:$true
}






############## Ablaufsteuerung ###########





# Auslesen der OU-Namen
$result | set-OUs
#Anlegen der Gruppen
$result | set-Groups



#remove-AllOUsAndGroups


