.. Powershell2AD documentation master file, created by
   sphinx-quickstart on Wed Aug 13 18:55:12 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Powershell2AD
=============

.. toctree::
   :maxdepth: 2
   
   gedanken.rst
   einleitung.rst
   hauptteil.rst



Index
=====

* :ref:`genindex` .. * :ref:`modindex`
* :ref:`search`

