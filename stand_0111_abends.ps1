
cls

# globale Variablen
#$DebugPreference = Continue


#OUs und Gruppen werden angelegt
#User anlegen und Gruppen zuweisen

$domaininfos = get-AdDomain
$DomainRoot = $domaininfos.DistinguishedName



$baseDn = "OU=Win2016_STE,"+ $domainRoot
#$baseDn = "steinam.local"

#Hashtable als Struktur f�r OUs
#Key = OUName, Value = DC=njd
$OU_HList = @{} #Aufnahme der Haupt-OUs
$OU_UList = @{} # Aufnahme der Unter-OUs
$Group_List= @{}




## csv-Datei einlesen
$csvresult = import-csv C:\temp\ad_struktur_eng.csv -Delimiter ","

write-Host $csvresult



## diverse Funktionen
function setOUs{

    #Vor Beginn der Pipe
    begin{
    Write-Host "lege OUs an"
    New-ADOrganizationalUnit -Name "Win2016_STE" -Path $DomainRoot -ProtectedFromAccidentalDeletion $False
        
    }

    #F�r jede Zeile in der Pipe
    process{
        
         #Objekte der Pipeline erhalten eine neue Eigenschaft
         #DistinguishedName 
     

        $Ou_Name = $_.HauptOU
        $OU_DN_Path = "" + $baseDn

        $OU_HList[$OU_Name]=$OU_DN_Path

        if($_.UnterOU -ne "")
        {
            $OU_DN_PATH = "OU="+$_.HauptOU+"," + $baseDn
            $Ou_Name = $_.UnterOU

            $OU_UList[$OU_Name]= $OU_DN_Path
 
        }


    }


    #Nach Ende der Pipe
    end{
        
        foreach($element in $OU_HList.Keys)
        {
        
            New-ADOrganizationalUnit -Name $element -Path $Ou_HList[$element] -ProtectedFromAccidentalDeletion $False
        
        } 
        
        
        foreach($element in $OU_UList.Keys)
        {
            New-ADOrganizationalUnit -Name $element -Path $Ou_UList[$element] -ProtectedFromAccidentalDeletion $False
        } 
        
        
        Write-Host "OUs hoffentlich angelegt"    
    }
}





function setGroups
{
    begin{
    
        $Group_List.Clear()
        New-ADOrganizationalUnit -Name "Gruppen" -Path $baseDN -ProtectedFromAccidentalDeletion $False
    }

    process{ 
        $Group_List[$_.Globale_Gruppe1]=$_.Globale_Gruppe1

        if($_.Globale_Gruppe2 -ne "")
        {
            $Group_List[$_.Globale_Gruppe2]=$_.Globale_Gruppe2
        }

        if($_.Globale_Gruppe3 -ne "")
        {
            $Group_List[$_.Globale_Gruppe3]=$_.Globale_Gruppe3
        }
    }

    end{   
        #New-ADGroup -name Personal.G -Path "OU=gruppen, OU= _CREATIVE, DC=CREATIV, DC=STE"
        $Group_List
         foreach($element in $Group_List.Keys)
        {
            $path = "OU=Gruppen, " + $baseDn
	        New-ADGroup -Name $Group_List[$element] -Path $path  -GroupScope Global
        }
    }
}


function  setUser
{
	begin{
	    Write-Debug "Adding User"
	}
	
	process{
        
        Add-Member -InputObject $_ -Name DName -Value "" -MemberType NoteProperty
        $_.DName = "OU="+ $_.HauptOU +"," + $baseDN	

        if($_.UnterOU -ne "")
        {
            $OU_DN_PATH = "OU="+$_.HauptOU+"," + $baseDn
          #  $Ou_Name = $_.UnterOU

           


            $_.DName = "OU=" +$_.UnterOU +"," + $OU_DN_PATH    
        }


        


    	$HomeDir = "C:\home\"+$_.Anmeldename
        Write-Host $HomeDir

        $password = "Test!!1234" | ConvertTo-SecureString -AsPlainText -Force
#       New-ADUser -Path $_.DName -SamAccountName $_.Anmeldename -Name $_.Name -AccountPassword (ConvertTo-SecureString -force -AsPlainText "Passw0rd") -ChangePasswordAtLogon $true	    
   
        New-ADUser -Path $_.DName -SamAccountName $_.Anmeldename -Name $_.Name -AccountPassword $password -ChangePasswordAtLogon $true -HomeDrive "H:" -HomeDirectory $HomeDir	    

}
	
	end{
	    write-debug "finished adding user"
	}
	
}



function setUserToGroups
{
    begin{
        Write-Host "Setze user auf Gruppen"

    }

    process{

        $myGroups = @()


        $myGroups += $_.Globale_Gruppe1
        if($_.Globale_Gruppe2 -ne "")
        {
            $myGroups += $_.Globale_Gruppe2
        }

        if($_.Globale_Gruppe3 -ne "")
        {
             $myGroups += $_.Globale_Gruppe3

        }


        foreach($gruppe in $myGroups)
        {
             Add-ADGroupMember $gruppe $_.Anmeldename 
         }

    }

    end{
    }
}


function set-HomeDir
{
    begin{

        $baseHomeDir = "C:\home\"
        mkdir $baseHomeDir
       

    }

    process{
       
        $homefolder = $baseHomeDir+$_.Anmeldename

        mkdir $homefolder
        <#
        #Access Control Liste f�r Homedirectory anzeigen lassen und in Variable $objACL schreiben
        $objACL = Get-Acl $homefolder
        #Neue Acces Controll Eintr�ge definieren
        $objACE = New-Object system.security.accesscontrol.filesystemaccessrule("$_.Anmeldename","Modify","ContainerInherit, ObjectInherit","None","Allow")
        #Bestehende Access Control Liste �ndern
        $objACL.SetAccessRule($objACE)
        #Neue Access Control Liste auf das Homeverzeichnis des Users anwenden
        Set-Acl $homefolder $objACL
        
#>


    }

    end{


    }



}



function remove-AllOUsAndGroups
{

     Remove-ADOrganizationalUnit -Identity $baseDn -Confirm:$False -recursive:$True    
     rmdir C:\home -Recurse     
   
}




#######Ablaufsteuerung########




function testpermissions
{


    $ACL = Get-ACL -Path "C:\Home\Mahn\"

    $User = get-AdUser "Mahn"

    $ruleDirection=[System.Security.AccessControl.AccessControlType]"Allow"



    #$groupsOrUser = [System.Security.Principal.NTAccount]"\\steinam\Mahn$"
    $fileSystemRights = [System.Security.AccessControl.FileSystemRights]"FullControl"
    $inherit = [system.security.accesscontrol.InheritanceFlags]"ContainerInherit, ObjectInherit"
    $propagation = [system.security.accesscontrol.PropagationFlags]"None"
   
    $accessrule = New-Object system.security.AccessControl.FileSystemAccessRule($User.SID, $fileSystemRights, $inherit, $propagation, $ruleDirection)
   
    $acl.AddAccessRule($accessrule)
   
    Set-ACL -Path "C:\Home\Mahn" -AclObject $ACL  

}



#        Pipe    Funktion
#remove-AllOUsAndGroups
#$csvresult | setOUs
#$csvresult | setGroups

#$csvresult | setUser

#$csvresult | setUserToGroups
#$csvresult | set-HomeDir

testpermissions

