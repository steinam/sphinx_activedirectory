Netzwerkadministration mit Windows PowerShell
=============================================


.. sidebar:: Link

	http://www.windowspro.de/script/get-aduser-set-aduser-benutzer-anlegen-abfragen-aendern-powershell

Für vielfältige Verwaltungsaufgaben in Windows-Netzwerken kann die Windows PowerShell eingesetzt werden. Es empfiehlt sich, die aktuelle PowerGUI, die einen komfortablen Skript Editor für die Powershell bietet, zu installieren. Die Powershell klinkt sich nahtlos in die Windowsumgebung ein und stellt über Snap-Ins den Zugriff auf das Active Directory von Windows Server 2008 R2 zur Verfügung

.. sidebar:: Hinweis

	Unter Win 2012 werden bei erstem Aufruf der AD-Commandlets diese automatisch importiert.
	
	.. image:: figure/ps_ad_modul_installieren.png
		:width: 150px

	.. image:: figure/ps_ad_commandlets.png
		:width: 150px

Mit diesen Zusatzmodulen kann die komplette Benutzer- und Sicherheitsverwaltung über Skripte geregelt werden, so dass große Usermengen in einem Rutsch automatisch in das Active Directory eingepflegt werden können.

Diese Option bietet gerade in großen Netzwerken eine erhebliche Erleichterung für die Einrichtung von Benutzern. Dazu gehören:

- Anlegen von Organisatorischen Einheiten
- Anlegen von globalen und lokalen Gruppen
- Anlage von Benutzern in der Datenbank mit allen notwendigen Eigenschaften (z. B. Name, Vorname, e-mail-Adresse)
- Hinzufügen von Benutzern zu den entsprechenden globalen Gruppen
- Erstellung der Homeverzeichnisse für die User
- Vergabe der Rechte für die Homeverzeichnisse


Überblick LDAP
--------------


Light Weight Directory Access Protocol ist ein internationaler Standard für die Verwaltung von Usern. Das erste Netzwerkbetriebssystem, das seine User nach LDAP verwaltet hat, war Novell Netware mit der NDS (Netware  Directory Services). Auch die User in Microsofts ADS (Active Directory Servcies) sind inzwischen nach diesem Standard angelegt. 

Das System ist dabei denkbar einfach: Ein User ist innerhalb einer Organisatorischen Einheit auf einem Domänen Controller angelegt. Beispielsweise haben wir den Benutzer Wolfgang Mahn innerhalb der Organisatorischen Einheit ``Geschaeftsleitung`` auf dem Domänencontroller ``Creativ.Local`` angelegt. Im internationalen Verzeichnisdienst LDAP sieht dass so aus:

CN=Wolfgang Mahn;OU=Geschaeftsleitung;DC=Creativ;DC=Local

Dabei stehen:
	
- CN für Common Name 
- OU für Organizational Unit
- DC für Domaincontext

	


Vorüberlegungen
---------------

Um die User möglichst einfach anlegen zu können, sollte vorher überlegt werden, welche Eigenschaften die User haben. Dazu gehören:
	
- Name
- Vorname
- Anmeldename
- Mitgliedschaft in einer Organisatorischen Einheiten (OU)
- Mitgliedschaft in den globalen Gruppen

Aus dem Organigramm der Creativ GmbH kann die folgende Übersicht entwickelt werden, die die Mitgliedschaft der User in den Organisatorischen Einheiten (OU) und globalen Gruppen veranschaulicht:

.. sidebar:: Nebeneffekte

	In der csv-Datei musste nach dem letzten Datensatz eine zusätzlicher Zeilenumbruch eingefügt werden

::
	
    
   Name	     Vorname   Anmeldename HauptOU	        UnterOU	         Globale Gruppe1        Globale Gruppe2	Globale Gruppe3
   Mahn	     Wolfgang  Mahn	   Geschaeftsleitung		         Geschaeftsleitung.G	Personal.G	
   Schneider Helga     Schneider   Geschaeftsleitung		         Personal.G		
   Zet	     Anne      Zet	   Geschaeftsleitung		         Personal.G		
   Lemmon    Sabine    Lemmon	   Auftragsbearbeitung		         Auftrag.G	        Management.G	
   Mall	     Tom       Mall	   Auftragsbearbeitung		         Auftrag.G		
   Schmidt   Peter     Schmidt	   Auftragsbearbeitung		         Auftrag.G		
   Wolz	     Werner    Wolz	   ServiceUndSupport	                 NetzwerkUndWerkstatt	Support.G	Management.G	
   Peters    Claus     Peters	   ServiceUndSupport	NetzwerkUWerkstatt	Support.G		
   Uhland    Bernd     Uhland	   ServiceUndSupport	NetzwerkUWerkstatt	Support.G		
   Inner     Hans      Inner	   ServiceUndSupport	MultiMedia	 Support.G	        Multimedia.G	
   Klein     Holger    Klein	   SoftwareUndTraining                   Software.G	        Management.G	        Training.G
   Fling     Elena     Fling	   SoftwareUndTraining	                 Software.G		
   Orfner    Dieter    Orfner	   SoftwareUndTraining	                 Software.G		
   Werner    Inge      Werner	   SoftwareUndTraining	                 Software.G	        Training.G	
	

Man kann in großen Netzwerken diese Daten direkt aus der Personaldatenbank gewinnen, um damit die User in die ADS einzupflegen. Dies wird später erläutert.


	