﻿$domainInfos = get-ADDomain
$baseDN = $domainInfos.DistinguishedName
$OU_HList = @{}
$OU_UList = @{}
$result = Import-Csv C:\sub_wc_old\docbook\sphinx_activedirectory\ad_struktur_eng.csv

$result

function set-OUPath
{
    
    process{
    
    
        Add-Member -InputObject $_ -Name OUPath -Value "" -MemberType NoteProperty
        if($_.UnterOU -eq "")
        {
            $_.OUPath = "OU="+$_.HauptOU+"," 
        }
        else
        {
            $_.OUPath = "OU=" + $_.UnterOu + ", OU="+$_.HauptOU+"," 
        }


        $OU_HList[$_.HauptOU] = "OU="+$_.HauptOU
        $OU_UList[$_.UnterOU] = "OU="+$_.UnterOU + ", OU=" + $_.HauptOU 
    }

}

$result | set-OUPath
#$result
$OU_HList
$OU_UList